`lish` is a LIsp SHell

# You

## might

### notice

1.  that

    1.  it's
    
        1.  not
        
            1.  quite \\\\
            
                1.  done \\\\
                
                    1.  yet.
                    
                        *Some* of the things it depends on are in:  
                        <https://github.com/nibbula/useless-pile-of-junk-with-a-catchy-name.git>

# When..

I'd like to stop using Bash. When I feel like I can do that, then perhaps
it will be the time to clean up the issues so other people can try to use it.

Also there's the dreaded ‘Utils’ issue!

```common-lisp
(defun org-xor (a b)
  "Exclusive or."
   (if a (not b) b))
```

I should probably add this as a ".md" since ".org" rendering is usually subpar.
Emacs can render to a ".md", but that's also subpar and needs hand tweaking.
__"*sigh*"__
